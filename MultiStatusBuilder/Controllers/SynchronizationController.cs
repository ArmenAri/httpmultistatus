﻿using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus;
using MultiStatusBuilder.Controllers.Addons;
using MultiStatusBuilder.DataTransferObjects;

namespace MultiStatusBuilder.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SynchronizationController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<MultiStatus<string>>> Get()
        {
            var dto1 = new SynchronizationResponse
            {
                Error = "Forbidden",
                CorrelationId = "CorrelationId1",
                DeviceId = "DeviceId1",
                StatusCode = HttpStatusCode.Forbidden
            };
            
            var dto2 = new SynchronizationResponse
            {
                Error = null,
                CorrelationId = "CorrelationId2",
                DeviceId = "DeviceI2"
            };

            return ControllerAddons.MultiStatus(new [] { dto1, dto2 });
        }
    }
}