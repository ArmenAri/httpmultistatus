﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using MultiStatusBuilder.Controllers.ActionResult;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus.Builder;

namespace MultiStatusBuilder.Controllers.Addons
{
    public static class ControllerAddons
    {
        /// <summary>
        /// Creates an <see cref="MultiStatusObjectResult"/> object that produces an <see cref="StatusCodes.Status207MultiStatus"/> response.
        /// </summary>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created <see cref="MultiStatusObjectResult"/> for the response.</returns>
        [NonAction]
        public static MultiStatusObjectResult MultiStatus<TKey, TSource>(
            [ActionResultObjectValue] IMultiStatusObject<TKey, TSource>? value)
        {
            return MultiStatus(new[] { value });
        }

        /// <summary>
        /// Creates an <see cref="MultiStatusObjectResult"/> object that produces an <see cref="StatusCodes.Status207MultiStatus"/> response.
        /// </summary>
        /// <param name="values">The content value to format in the entity body.</param>
        /// <returns>The created <see cref="MultiStatusObjectResult"/> for the response.</returns>
        [NonAction]
        public static MultiStatusObjectResult MultiStatus<TKey, TSource>(
            [ActionResultObjectValue] IEnumerable<IMultiStatusObject<TKey, TSource>> values)
        {
            var valueList = values.ToList();
            
            if (valueList.Count > 0)
            {
                return new MultiStatusObjectResult(valueList
                    .Aggregate(
                        MultiStatusBuilderPool<TKey, TSource>.TryGetBuilder(valueList[0]), 
                        (acc, x) => acc.Add(x, x.StatusCode))
                    .Build());
            }
            
            return new MultiStatusObjectResult(values);
        }
    }
}