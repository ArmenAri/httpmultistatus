﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace MultiStatusBuilder.Controllers.ActionResult
{
    /// <summary>
    /// An <see cref="ObjectResult"/> that when executed performs content negotiation, formats the entity body, and
    /// will produce a <see cref="StatusCodes.Status207MultiStatus"/> response if negotiation and formatting succeed.
    /// </summary>
    [DefaultStatusCode(DefaultStatusCode)]
    public class MultiStatusObjectResult : ObjectResult
    {
        private const int DefaultStatusCode = StatusCodes.Status207MultiStatus;

        /// <summary>
        /// Initializes a new instance of the <see cref="OkObjectResult"/> class.
        /// </summary>
        /// <param name="value">The content to format into the entity body.</param>
        public MultiStatusObjectResult(object? value)
            : base(value)
        {
            StatusCode = DefaultStatusCode;
        }
    }   
}