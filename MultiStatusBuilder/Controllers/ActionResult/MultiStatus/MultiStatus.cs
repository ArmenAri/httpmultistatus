﻿using System.Collections.Generic;

namespace MultiStatusBuilder.Controllers.ActionResult.MultiStatus
{
    public sealed class MultiStatus<T>
    {
        public T ResourceId { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public IDictionary<string, object> AdditionalInformation { get; set; }
    }
}