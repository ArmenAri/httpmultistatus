﻿namespace MultiStatusBuilder.Controllers.ActionResult.MultiStatus
{
    public class Metadata
    {
        public int Succeed { get; set; }
        public int Failed { get; set; }
        public int Total { get; set; }
    }
}