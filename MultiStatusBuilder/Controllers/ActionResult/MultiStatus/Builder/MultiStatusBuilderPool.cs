﻿using System;
using System.Collections.Generic;

namespace MultiStatusBuilder.Controllers.ActionResult.MultiStatus.Builder
{
    public static class MultiStatusBuilderPool<TKey, TSource>
    {
        private static readonly IDictionary<Type, MultiStatusBuilder<TKey, TSource>> Builders = new Dictionary<Type, MultiStatusBuilder<TKey, TSource>>();

        public static MultiStatusBuilder<TKey, TSource> TryGetBuilder(IMultiStatusObject<TKey, TSource> value)
        {
            var builderKey = typeof(TSource);
            
            if (Builders.TryGetValue(builderKey, out var builder))
            {
                return builder;
            }
            
            var newBuilder = value.ConfigureMultiStatusBuilder();
            Builders.Add(builderKey, newBuilder);
            
            return newBuilder;
        }
    }
}