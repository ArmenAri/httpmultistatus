﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using Mapper = AutoMapper.Mapper;
using MapperConfiguration = AutoMapper.MapperConfiguration;

namespace MultiStatusBuilder.Controllers.ActionResult.MultiStatus.Builder
{
    public sealed class MultiStatusBuilder<TKey, TSource>
    {
        private List<MultiStatus<TKey>> Results { get; }
        private Mapper Mapper { get; }

        public MultiStatusBuilder(MultiStatusBuilderConfiguration<TKey, TSource> configuration)
        {
            Results = new List<MultiStatus<TKey>>();
            Mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<TSource, MultiStatus<TKey>>()
                .ForMember(dest => dest.ResourceId, member => member.MapFrom(configuration.ResourceIdentifier))
                .ForMember(dest => dest.Status,
                    member => member.MapFrom(src =>
                        configuration.SuccessStatusCodeSelector(src) ? (int) configuration.DefaultSuccessStatusCode : -1))
                .ForMember(dest => dest.Message, member => member.MapFrom(configuration.MessageIdentifier))
                .ForMember(dest => dest.AdditionalInformation,
                    member => member.MapFrom(configuration.AdditionalInformation))
            ));
        }

        public MultiStatusBuilder<TKey, TSource> Add(IMultiStatusObject<TKey, TSource> @object, HttpStatusCode? statusCode = null)
        {
            var multiStatus = Mapper.Map<MultiStatus<TKey>>(@object);

            if (statusCode != null)
            {
                multiStatus.Status = (int)statusCode;
            }

            Results.Add(multiStatus);
            return this;
        }

        public MultiStatusResponse<TKey> Build()
        {
            var result = new List<MultiStatus<TKey>>(Results);

            var succeedCount = Results.Count(r => r.Status >= 200 && r.Status < 300);
            
            var metadata = new Metadata
            {
                Succeed = succeedCount,
                Failed = Results.Count - succeedCount,
                Total = Results.Count
            };
            
            Results.Clear();
            return new MultiStatusResponse<TKey>
            {
                Results = result,
                Metadata = metadata
            };
        }
    }

    public sealed class MultiStatusBuilderConfiguration<TKey, TSource>
    {
        public HttpStatusCode DefaultSuccessStatusCode { get; set; } = HttpStatusCode.OK;
        public Func<TSource, bool> SuccessStatusCodeSelector { get; set; }
        public Expression<Func<TSource, TKey>> ResourceIdentifier { get; set; }
        public Expression<Func<TSource, string>> MessageIdentifier { get; set; }
        public Expression<Func<TSource, IDictionary<string, object>>> AdditionalInformation { get; set; }
    }

    public sealed class MultiStatusResponse<TKey>
    {
        public List<MultiStatus<TKey>> Results { get; set; }
        public Metadata Metadata { get; set; }
    }
}