﻿using System.Net;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus.Builder;

namespace MultiStatusBuilder.Controllers.ActionResult.MultiStatus
{
    public interface IMultiStatusObject<TKey, TSource>
    {
        public MultiStatusBuilder<TKey, TSource> ConfigureMultiStatusBuilder();
        HttpStatusCode? StatusCode { get; set; }
    }
}