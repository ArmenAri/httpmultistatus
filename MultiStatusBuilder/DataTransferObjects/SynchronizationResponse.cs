﻿using System.Collections.Generic;
using System.Net;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus;
using MultiStatusBuilder.Controllers.ActionResult.MultiStatus.Builder;

namespace MultiStatusBuilder.DataTransferObjects
{
    public class SynchronizationResponse : IMultiStatusObject<string, SynchronizationResponse>
    {
        public string CorrelationId { get; set; }
        public string DeviceId { get; set; }
        public string Error { get; set; }
        
        public MultiStatusBuilder<string, SynchronizationResponse> ConfigureMultiStatusBuilder()
        {
            return new MultiStatusBuilder<string, SynchronizationResponse>(
                new MultiStatusBuilderConfiguration<string, SynchronizationResponse>
                {
                    DefaultSuccessStatusCode = HttpStatusCode.Created,
                    ResourceIdentifier = src => src.DeviceId,
                    SuccessStatusCodeSelector = src => src.Error == null,
                    MessageIdentifier = src => src.Error,
                    AdditionalInformation = src => new Dictionary<string, object>
                    {
                        { "CorrelationId", src.CorrelationId }
                    }
                });
        }

        public HttpStatusCode? StatusCode { get; set; }
    }
}